$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 50, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/nbrvat/taxtarrif/data-table", // ajax source
            'data': function(data){

                var hscode = $('#hscode').val();
                var name = $('#name').val();
                var customsDuty = $('#customsDuty').val();
                var supplementaryDuty = $('#supplementaryDuty').val();
                var valueAddedTax = $('#valueAddedTax').val();
                var advanceIncomeTax = $('#advanceIncomeTax').val();
                var recurringDeposit = $('#recurringDeposit').val();
                var advanceTradeVat = $('#advanceTradeVat').val();
                var totalTaxIncidence = $('#totalTaxIncidence').val();


                // Append to data
                //  data._token = CSRF_TOKEN;

                data.hscode = hscode;
                data.name = name;
                data.customsDuty = customsDuty;
                data.name = name;
                data.supplementaryDuty = supplementaryDuty;
                data.valueAddedTax = valueAddedTax;
                data.advanceIncomeTax = advanceIncomeTax;
                data.recurringDeposit = recurringDeposit;
                data.advanceTradeVat = advanceTradeVat;
                data.totalTaxIncidence = totalTaxIncidence;

            }
        },
        'columns': [
            { "name": 'id',width:"2%",'orderable':false},
            { "name": 'hscode',width:"10%"},
            { "name": 'name',width:"50%"},
            { "name": 'customsDuty','orderable':false},
            { "name": 'supplementaryDuty','orderable':false },
            { "name": 'valueAddedTax','orderable':false },
            { "name": 'advanceIncomeTax','orderable':false },
            { "name": 'recurringDeposit','orderable':false },
            { "name": 'advanceTradeVat','orderable':false },
            { "name": 'totalTaxIncidence','orderable':false}


        ],
        "order": [
            [1, "asc"]
        ]

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#hscode').change(function(){
        dataTable.draw();
    });


});

