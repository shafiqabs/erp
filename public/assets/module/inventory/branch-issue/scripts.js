$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });


    $(document).on('change', '#product', function() {

        var url = $('#product').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                obj = JSON.parse(response);
                $('#productId').val(obj['productId']);
                $('#salesPrice').val(obj['salesPrice']);
                $('#price').html(obj['salesPrice']);
                $('#unit').html(obj['unit']);
                $('#remaining').html(obj['remainingQnt']);
                $('#remainingQnt').val(obj['remainingQnt']);
                $('#valueAddedTaxPercent').val(obj['valueAddedTaxPercent']);
                $('#batchItem').html(obj['batchItems']);
            }
        })
    });

    $(document).on('change', '#productionBatch', function() {
        var url = $(this).attr('data-action');
        id =  $(this).val();
        if(id === ""){
            return false;
        }
        $.get( url,{item:id})
            .done(function(data) {
                $('#remaining').html(data);
                $('#batchQnt').val(data);
            });

    });


    $(document).on('click', '#addProduct', function() {

        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var productionBatch = $('#productionBatch').val();
        var batchQnt = $('#batchQnt').val();
        var valueAddedTaxPercent = $('#valueAddedTaxPercent').val();
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Quantity must be required");
            $('#quantity').focus();
            return false;
        }

        if(parseFloat(quantity) > parseFloat(batchQnt)){
            $('#quantity').focus();
            $.MessageBox("Sales quantity must be less/equal form the remaining quantity");
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity+'&valueAddedTaxPercent='+valueAddedTaxPercent+'&productionBatch='+productionBatch,
            success: function (response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
            }
        })
    });


    $(document).on('change', '.quantity', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var maxQuantity = parseFloat($('#quantity-'+id).attr('max'));
        var salesPrice = parseFloat($('#salesPrice-'+id).val());
        var vatPercent = parseFloat($('#valueAddedTaxPercent-'+id).val());
        var subTotal  = (quantity * salesPrice);
        var vat  = ((subTotal * vatPercent)/100);
        $("#subTotal-"+id).html(subTotal);
        $("#vatTotal-"+id).html(vat);
        $("#total-"+id).html(subTotal + vat);
        if(parseFloat(quantity) > parseFloat(maxQuantity)){
            $('#quantity-'+id).focus();
            $.MessageBox("Sales quantity must be less/equal form the remaining quantity");
            return false;
        }
        $.ajax({
            "url":  "/"+lang+"/inventory/branch-issue/item-update",
            type: 'POST',
            data:'item='+id+'&quantity='+quantity+'&salesPrice='+salesPrice+"&valueAddedTaxPercent="+vatPercent,
            success: function(response) {
                obj = JSON.parse(response);
                setTimeout(jsonResult(response),100);
            }

        })
    });

    $(document).on('click',".item-remove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonDone  : "Yes",
            buttonFail  : "No",
            message     : "Are you sure you want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                $(event.target).closest('tr').hide();
                setTimeout(jsonResult(response),100);
            });
        });
    });

    function jsonResult(response) {

        obj = JSON.parse(response);
        $('#invoiceItems').html(obj['invoiceItems']);
        $('#totalQuantity').html(obj['totalQuantity']);
        $('#subTotal').html(obj['subTotal']);
        $('#totalVat').html(obj['totalVat']);
        $('#totalTti').html(obj['totalTti']);
        $('#total').html(obj['total']);


    }


    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.payment', function (e) {
        var mrp = $(this).val();
        var netTotal = Number.parseFloat($("#paymentTotal").val());
        $('#payable').html(financial(mrp));
        due = financial(netTotal - mrp);
        $('#duable').html(due);
    });



    $(document).on('change', '.branch', function () {

        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function () {
                location.reload();
            }
        });

    });

    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

});



