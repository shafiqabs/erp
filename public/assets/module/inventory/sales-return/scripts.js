
$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });


    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.quantity', function (e) {

        var quantity = $(this).val();
        var id = $(this).attr('data-id');
        var max = $(this).attr('max');
        if(parseFloat(quantity) > parseFloat(max) ){
            $.MessageBox("Return quantity must be less or equal from the remaining quantity");
            $('#quantity-'+id).val('').focusin();
            return false;
        }

    });

    $(document).on('change', '.salesId', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function () {
                location.reload();
            }
        });
    });

});



