$(document).ready(function () {

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":Routing.generate('admin_location_data_table'), // ajax source
            'data': function(data){

                var name = $('#name').val();
                var district = $('#district').val();
                data.name = name;
                data.district = district;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'district' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [3], "sClass":  "text-center"}
        ],
        "order": [
            [2, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 3,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#district').keyup(function(){
        dataTable.draw();
    });


});

