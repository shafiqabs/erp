$(document).ready(function () {

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)')
            .exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength":50, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url":  "/"+lang+"/nbrvat/vds/data-vds-table", // ajax source
            'data': function(data){

                var created = $('#created').val();
                var invoice = $('#invoice').val();
                var companyName = $('#companyName').val();
                var total = $('#total').val();
                var payment = $('#payment').val();
                var balance = $('#balance').val();
                var mode = $('#mode').val();
                var process = $('#process').val();
                // Append to data
                //  data._token = CSRF_TOKEN;

                data.created = created;
                data.invoice = invoice;
                data.companyName = companyName;
                data.total = total;
                data.payment = payment;
                data.balance = balance;
                data.mode = mode;
                data.process = process;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'created' },
            { "name": 'invoice' },
            { "name": 'companyName' },
            { "name": 'total','orderable':false},
            { "name": 'payment','orderable':false  },
            { "name": 'balance','orderable':false  },
            { "name": 'mode' },
            { "name": 'process' },
            { "name": 'action','orderable':false,"sClass":  "text-center"}

        ],
        "order": [
            [1, "asc"]
        ]

    });

    $('#vendor').keyup(function(){
        dataTable.draw();
    });
    $('#startDate').change(function(){
        dataTable.draw();
    });
    $('#endDate').change(function(){
        dataTable.draw();
    });

    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url);
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

});

