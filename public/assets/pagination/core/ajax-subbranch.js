$(document).ready(function () {
    var table = $('#datatable').DataTable( {
        "ajax": {
            "type"   : "POST",
            "processing": true,
            "serverSide": true,
            "url": Routing.generate('domain_subbranch_data_table')
        },
        "initComplete": function() {
            $('.status').bootstrapToggle()
        },
        pageLength: 25,
        // scrollY:'100vh',
        // scrollCollapse: true,
        paging: true,
        order: [ 0, 'asc' ]
    });

});


