<?php

/*
 * This file is part of the Docudex project.
 *
 * (c) Devnet Limited <http://www.devnetlimited.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Core;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Domain\Customer;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**

 * @ORM\Table(name="core_employee")
 * @ORM\Entity(repositoryClass="App\Repository\Core\EmployeeRepository")
 * @UniqueEntity(fields="employeeId",message="This employee id must be unique")
 * @ORM\HasLifecycleCallbacks
 */
class Employee
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="fatherName", type="string", nullable=true)
     */
    private $fatherName;

    /**
     * @var string
     *
     * @ORM\Column(name="motherName", type="string", nullable=true)
     */
    private $motherName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $designation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $operationalDesignation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $department;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    protected $company;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $bank;


    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $section;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $category;


    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=15, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $bankName;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $branchName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $bankAccountNo;


     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $routingNo;


     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ebl;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $eblBankName = "Eastern Bank Ltd";


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $eblBranchName;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $eblRoutingNo;


     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $doj;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;


    /**
     * @var text
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;


    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", nullable=true)
     */
    private $postalCode;



    /**
     * @var string
     *
     * @ORM\Column(name="nid", type="string", nullable=true)
     */
    private $nid;


    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    private $gender;


    /**
     * @var string
     *
     * @ORM\Column(name="bloodGroup", type="string", nullable=true)
     */
    private $bloodGroup;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $employeeId;



    /**
     * @var string
     *
     * @ORM\Column(name="employeeType", type="string", nullable=true)
     */
    private $employeeType;


    /**
     * @var string
     *
     * @ORM\Column(name="joiningDate", type="datetime", nullable=true)
     */
    private $joiningDate;

    /**
     * @var string
     *
     * @ORM\Column(name="leaveDate", type="datetime", nullable=true)
     */
    private $leaveDate;


    /**
     * @var string
     *
     * @ORM\Column(name="accountNo", type="string", length=255, nullable = true)
     */
    private $accountNo;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $effectiveDate;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $salary;



    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $leaveStatus;



    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $accountNumber;


     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $removeImage = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $signaturePath;


    /**
     * @Assert\File(maxSize="1M")
     */
    protected $file;

     /**
     * @Assert\File(maxSize="1M")
     */
    protected $signatureFile;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Sets file.
     *
     * @param Profile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Profile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets signatureFile.
     *
     * @param Profile $signatureFile
     */
    public function setSignatureFile(UploadedFile $signatureFile = null)
    {
        $this->signatureFile = $signatureFile;
    }

    /**
     * Get signatureFile.
     *
     * @return Profile
     */
    public function getSignatureFile()
    {
        return $this->signatureFile;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getAbsoluteSignaturePath()
    {
        return null === $this->signaturePath
            ? null
            : $this->getUploadRootDir().'/'.$this->signaturePath;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/user/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath() and  file_exists($this->getAbsolutePath())) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function removeSignatureUpload()
    {
        if ($file = $this->getAbsoluteSignaturePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    public function signatureUpload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getSignatureFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getSignatureFile()->getClientOriginalName();
        $this->getSignatureFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->signaturePath = $filename ;

        // clean up the file property as you won't need it anymore
        $this->signatureUpload = null;
    }

    /**
     * @return boolean
     */
    public function getRemoveImage()
    {
        return $this->removeImage;
    }

    /**
     * @param boolean $removeImage
     */
    public function setRemoveImage($removeImage)
    {
        $this->removeImage = $removeImage;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNameWithEmployeeId()
    {
        return $this->employeeId." - ".$this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @return text
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param text $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getBloodGroup()
    {
        return $this->bloodGroup;
    }

    /**
     * @param string $bloodGroup
     */
    public function setBloodGroup($bloodGroup)
    {
        $this->bloodGroup = $bloodGroup;
    }



    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getNid()
    {
        return $this->nid;
    }

    /**
     * @param string $nid
     */
    public function setNid($nid)
    {
        $this->nid = $nid;
    }



    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getJoiningDate()
    {
        return $this->joiningDate;
    }

    
    /**
     * @param string $joiningDate
     */
    public function setJoiningDate($joiningDate)
    {
        $this->joiningDate = $joiningDate;
    }

    /**
     * @return string
     */
    public function getLeaveDate()
    {
        return $this->leaveDate;
    }

    /**
     * @param string $leaveDate
     */
    public function setLeaveDate($leaveDate)
    {
        $this->leaveDate = $leaveDate;
    }


    /**
     * @return string
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param string $accountNo
     */
    public function setAccountNo($accountNo)
    {
        $this->accountNo = $accountNo;
    }



    /**
     * @return string
     */
    public function getFatherName()
    {
        return $this->fatherName;
    }

    /**
     * @param string $fatherName
     */
    public function setFatherName($fatherName)
    {
        $this->fatherName = $fatherName;
    }

    /**
     * @return string
     */
    public function getMotherName()
    {
        return $this->motherName;
    }

    /**
     * @param string $motherName
     */
    public function setMotherName($motherName)
    {
        $this->motherName = $motherName;
    }


    /**
     * @return string
     */
    public function getEmployeeType()
    {
        return $this->employeeType;
    }

    /**
     * @param string $employeeType
     */
    public function setEmployeeType($employeeType)
    {
        $this->employeeType = $employeeType;
    }



    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }


    /**
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string $dateOfBirth
     */
    public function setDateOfBirth(string $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @param string $effectiveDate
     */
    public function setEffectiveDate(string $effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param string $salary
     */
    public function setSalary(string $salary)
    {
        $this->salary = $salary;
    }



    /**
     * @return string
     */
    public function getLeaveStatus()
    {
        return $this->leaveStatus;
    }

    /**
     * @param string $leaveStatus
     */
    public function setLeaveStatus(string $leaveStatus)
    {
        $this->leaveStatus = $leaveStatus;
    }

    /**
     * @return Bank
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param Bank $bank
     */
    public function setBank(Bank $bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber(string $accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }



    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Setting
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param Setting $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return Setting
     */
    public function getOperationalDesignation()
    {
        return $this->operationalDesignation;
    }

    /**
     * @param Setting $operationalDesignation
     */
    public function setOperationalDesignation($operationalDesignation)
    {
        $this->operationalDesignation = $operationalDesignation;
    }

    /**
     * @return string
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * @param string $employeeId
     */
    public function setEmployeeId(string $employeeId)
    {
        $this->employeeId = $employeeId;
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getSignaturePath()
    {
        return $this->signaturePath;
    }

    /**
     * @param string $signaturePath
     */
    public function setSignaturePath(string $signaturePath)
    {
        $this->signaturePath = $signaturePath;
    }

    /**
     * @return Setting
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param Setting $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return Setting
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Setting $category
     */
    public function setCategory(Setting $category)
    {
        $this->category = $category;
    }


    /**
     * @return string
     */
    public function getBranchName()
    {
        return $this->branchName;
    }

    /**
     * @param string $branchName
     */
    public function setBranchName($branchName)
    {
        $this->branchName = $branchName;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param string $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getDoj()
    {
        return $this->doj;
    }

    /**
     * @param string $doj
     */
    public function setDoj(string $doj)
    {
        $this->doj = $doj;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return string
     */
    public function getEbl()
    {
        return $this->ebl;
    }

    /**
     * @param string $ebl
     */
    public function setEbl($ebl)
    {
        $this->ebl = $ebl;
    }


    /**
     * @return string
     */
    public function getRoutingNo()
    {
        return $this->routingNo;
    }

    /**
     * @param string $routingNo
     */
    public function setRoutingNo($routingNo)
    {
        $this->routingNo = $routingNo;
    }

    /**
     * @return string
     */
    public function getEblBankName()
    {
        return $this->eblBankName;
    }

    /**
     * @param string $eblBankName
     */
    public function setEblBankName($eblBankName)
    {
        $this->eblBankName = $eblBankName;
    }

    /**
     * @return string
     */
    public function getEblBranchName()
    {
        return $this->eblBranchName;
    }

    /**
     * @param string $eblBranchName
     */
    public function setEblBranchName( $eblBranchName)
    {
        $this->eblBranchName = $eblBranchName;
    }

    /**
     * @return string
     */
    public function getEblRoutingNo()
    {
        return $this->eblRoutingNo;
    }

    /**
     * @param string $eblRoutingNo
     */
    public function setEblRoutingNo( $eblRoutingNo)
    {
        $this->eblRoutingNo = $eblRoutingNo;
    }
    
    



}