<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Application;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\NbrvatBundle\Entity\TaxSetup;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Application\BudgetRepository")
 * @ORM\Table(name="tbd_budget")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Budget
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $terminal;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $budgetMergeLimit = 0;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status = 0;


     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isStatus(): ? bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return int
     */
    public function getBudgetMergeLimit()
    {
        return $this->budgetMergeLimit;
    }

    /**
     * @param int $budgetMergeLimit
     */
    public function setBudgetMergeLimit(int $budgetMergeLimit)
    {
        $this->budgetMergeLimit = $budgetMergeLimit;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }




}
