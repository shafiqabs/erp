<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity\Domain;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Core\Setting;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\VendorRepository")
 * @ORM\Table(name="domain_vendor")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Vendor
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="terminal", type="integer" , nullable=true)
     */
    private $terminal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     **/
    protected $location;


    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    protected $businessType;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    protected $vendorType;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    protected $relatedVendor;

    /**
     * @var Bank
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
     **/
    protected $bank;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     **/
    protected $department;


     /**
     * @var VendorCreditDayLimit
     * @ORM\OneToMany(targetEntity="App\Entity\Domain\VendorCreditDayLimit" , mappedBy="vendor")
     **/
    protected $creditLimits;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 50, nullable=true)
     */
    private $registrationNo;

     /**
     * @var string
     *
     * @ORM\Column(type="string", length = 100, nullable=true)
     */
    private $vendorId;

     /**
     * @var string
     *
     * @ORM\Column(type="string", length = 100, nullable=true)
     */
    private $vendorCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string",  nullable=true)
     */
    private $accountName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $accountNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $branchName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 100, nullable=true)
     */
    private $accountType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 50, nullable=true)
     */
    private $vatRegistrationNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 50, nullable=true)
     */
    private $tinNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 50, nullable=true)
     */
    private $binNo;


     /**
     * @var string
     *
     * @ORM\Column(type="string", length = 100, nullable=true)
     */
    private $routingNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ebl;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $eblBankName = "Eastern Bank Ltd";


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $eblBranchName;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $eblRoutingNo;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 100, nullable=true)
     */
    private $tradeLicenseNo;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $contactPerson;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $skype;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $altEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length = 50, nullable=true)
     */
    private $nid;


    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer", nullable=true)
     */
    private $code;


    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string", length=255 , nullable=true)
     */
    private $companyName;

    /**
     * @Gedmo\Slug(fields={"companyName"})
     * @Doctrine\ORM\Mapping\Column(length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $altMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $altPhone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fax;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255 , nullable=true)
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length= 50, nullable=true)
     */
    private $mode;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true )
     */
    private $relatedParty = false;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true )
     */
    private $status=true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }



    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


    /**
     * @return string
     */
    public function getMode(){
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode ) {
        $this->mode = $mode;
    }


    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getPhone(): ? string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAltPhone():? string
    {
        return $this->altPhone;
    }

    /**
     * @param string $altPhone
     */
    public function setAltPhone(string $altPhone)
    {
        $this->altPhone = $altPhone;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getAltEmail()
    {
        return $this->altEmail;
    }

    /**
     * @param string $altEmail
     */
    public function setAltEmail(string $altEmail)
    {
        $this->altEmail = $altEmail;
    }

    /**
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param string $skype
     */
    public function setSkype(string $skype)
    {
        $this->skype = $skype;
    }

    /**
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook(string $facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getBinNo(): ? string
    {
        return $this->binNo;
    }

    /**
     * @param string $binNo
     */
    public function setBinNo(string $binNo)
    {
        $this->binNo = $binNo;
    }

    /**
     * @return string
     */
    public function getVatRegistrationNo(): ? string
    {
        return $this->vatRegistrationNo;
    }

    /**
     * @param string $vatRegistrationNo
     */
    public function setVatRegistrationNo(string $vatRegistrationNo)
    {
        $this->vatRegistrationNo = $vatRegistrationNo;
    }

    /**
     * @return string
     */
    public function getRegistrationNo():? string
    {
        return $this->registrationNo;
    }

    /**
     * @param string $registrationNo
     */
    public function setRegistrationNo(string $registrationNo)
    {
        $this->registrationNo = $registrationNo;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): ? string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile( $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax(string $fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getAltMobile()
    {
        return $this->altMobile;
    }

    /**
     * @param string $altMobile
     */
    public function setAltMobile( $altMobile)
    {
        $this->altMobile = $altMobile;
    }

    /**
     * @return Setting
     */
    public function getVendorType(): ? Setting
    {
        return $this->vendorType;
    }

    /**
     * @param Setting $vendorType
     */
    public function setVendorType(Setting $vendorType)
    {
        $this->vendorType = $vendorType;
    }

    /**
     * @return Setting
     */
    public function getBusinessType(): ? Setting
    {
        return $this->businessType;
    }

    /**
     * @param Setting $businessType
     */
    public function setBusinessType(Setting $businessType)
    {
        $this->businessType = $businessType;
    }

    /**
     * @return string
     */
    public function getNid(): ? string
    {
        return $this->nid;
    }

    /**
     * @param string $nid
     */
    public function setNid(string $nid)
    {
        $this->nid = $nid;
    }

    /**
     * @return int
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param int $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Bank
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param Bank $bank
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * @param string $accountName
     */
    public function setAccountName(string $accountName)
    {
        $this->accountName = $accountName;
    }

    /**
     * @return string
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param string $accountNo
     */
    public function setAccountNo(string $accountNo)
    {
        $this->accountNo = $accountNo;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
        return $this->branchName;
    }

    /**
     * @param string $branchName
     */
    public function setBranchName(string $branchName)
    {
        $this->branchName = $branchName;
    }

    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param string $accountType
     */
    public function setAccountType( $accountType)
    {
        $this->accountType = $accountType;
    }

    /**
     * @return string
     */
    public function getTinNo()
    {
        return $this->tinNo;
    }

    /**
     * @param string $tinNo
     */
    public function setTinNo($tinNo)
    {
        $this->tinNo = $tinNo;
    }

    /**
     * @return bool
     */
    public function isRelatedParty()
    {
        return $this->relatedParty;
    }

    /**
     * @param bool $relatedParty
     */
    public function setRelatedParty($relatedParty)
    {
        $this->relatedParty = $relatedParty;
    }

    /**
     * @return Setting
     */
    public function getRelatedVendor()
    {
        return $this->relatedVendor;
    }

    /**
     * @param Setting $relatedVendor
     */
    public function setRelatedVendor($relatedVendor)
    {
        $this->relatedVendor = $relatedVendor;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     */
    public function setContactPerson( $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return string
     */
    public function getRoutingNo()
    {
        return $this->routingNo;
    }

    /**
     * @param string $routingNo
     */
    public function setRoutingNo($routingNo)
    {
        $this->routingNo = $routingNo;
    }

    /**
     * @return string
     */
    public function getTradeLicenseNo()
    {
        return $this->tradeLicenseNo;
    }

    /**
     * @param string $tradeLicenseNo
     */
    public function setTradeLicenseNo($tradeLicenseNo)
    {
        $this->tradeLicenseNo = $tradeLicenseNo;
    }

    /**
     * @return VendorCreditDayLimit
     */
    public function getCreditLimits()
    {
        return $this->creditLimits;
    }

    /**
     * @return string
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param string $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }

    /**
     * @return string
     */
    public function getEbl()
    {
        return $this->ebl;
    }

    /**
     * @param string $ebl
     */
    public function setEbl($ebl)
    {
        $this->ebl = $ebl;
    }


    /**
     * @return string
     */
    public function getVendorCode()
    {
        return $this->vendorCode;
    }

    /**
     * @param string $vendorCode
     */
    public function setVendorCode($vendorCode)
    {
        $this->vendorCode = $vendorCode;
    }

    /**
     * @return string
     */
    public function getEblBankName()
    {
        return $this->eblBankName;
    }

    /**
     * @param string $eblBankName
     */
    public function setEblBankName($eblBankName)
    {
        $this->eblBankName = $eblBankName;
    }

    /**
     * @return string
     */
    public function getEblBranchName()
    {
        return $this->eblBranchName;
    }

    /**
     * @param string $eblBranchName
     */
    public function setEblBranchName( $eblBranchName)
    {
        $this->eblBranchName = $eblBranchName;
    }

    /**
     * @return string
     */
    public function getEblRoutingNo()
    {
        return $this->eblRoutingNo;
    }

    /**
     * @param string $eblRoutingNo
     */
    public function setEblRoutingNo( $eblRoutingNo)
    {
        $this->eblRoutingNo = $eblRoutingNo;
    }





}
