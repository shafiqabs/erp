<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Entity\Domain;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Core\Setting;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Domain\VendorCreditDayLimitRepository")
 * @ORM\Table(name="domain_vendor_company_day_limit")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VendorCreditDayLimit
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var Vendor
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Vendor", inversedBy="creditLimits")
     **/
    protected $vendor;


    /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     **/
    protected $company;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $limitDay;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true )
     */
    private $status=true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return Branch
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Branch $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getLimitDay()
    {
        return $this->limitDay;
    }

    /**
     * @param int $limitDay
     */
    public function setLimitDay($limitDay)
    {
        $this->limitDay = $limitDay;
    }




}
