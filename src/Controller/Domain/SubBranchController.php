<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;

use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Entity\Domain\Branch;
use App\Form\Core\SettingFormType;
use App\Form\Domain\BranchFormType;
use App\Form\Domain\SubBranchFormType;
use App\Repository\Core\ItemKeyValueRepository;
use App\Repository\Core\SettingRepository;
use App\Repository\Core\SettingTypeRepository;
use App\Repository\Domain\BranchRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/domain/sub-branch")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SubBranchController extends AbstractController
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="domain_subbranch")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, SettingRepository $settingRepository, ItemKeyValueRepository $keyValueRepository, SettingTypeRepository $settingTypeRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = new Branch();
        $data = $request->request->all();
        $form = $this->createForm(SubBranchFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTerminal($terminal);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('domain_subbranch');
        }
        return $this->render('domain/subbranch/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="domain_subbranch_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, BranchRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            'domain/subbranch/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="domain_subbranch_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, BranchRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="domain_branch_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id , BranchRepository $branchRepository, TranslatorInterface $translator): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        /* @var $entity Branch */
        $entity = $branchRepository->findOneBy(array('terminal'=>$terminal->getId(),'id'=>$id));
        $form = $this->createForm(SubBranchFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('domain_subbranch');
        }
        return $this->render('domain/subbranch/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="domain_subbranch_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, BranchRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = $repository->findOneBy(array('terminal'=>$terminal,'id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }


    /**
     * @Route("/sub-branch-data-table", methods={"GET", "POST"}, name="domain_subbranch_data_table" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function dataReportingTable(Request $request, BranchRepository $branchRepository)
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $entities = $branchRepository->getSubRecords($terminal);
        $iTotalRecords = $branchRepository->count(array('terminal'=> $terminal));

        $i = 1;
        $records = array();
        $records["data"] = array();

        foreach ($entities as $post):

            $active = empty($post['status']) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('domain_subbranch_status',array('id'=>$post['id']))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";

            $records["data"][] = array(
                $id                 = $i,
                $branch               = $post['branch'],
                $name               = $post['name'],
                $code               = $post['code'],
                $location           = $post['location'],
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('domain_branch_edit',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('domain_subbranch_show',array('id'=>$post['id']))}' href='javascript:' data-title='{$post['name']}' id='postView-{$post['id']}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:' data-action='{$this->generateUrl('domain_subbranch_delete',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;
        endforeach;
        return new JsonResponse($records);
    }



}
