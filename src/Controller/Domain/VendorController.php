<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;
use App\Entity\Domain\Vendor;
use App\Form\Domain\VendorFormType;
use App\Repository\Core\SettingRepository;
use App\Repository\Domain\VendorRepository;
use App\Service\ConfigureManager;
use App\Service\FormValidationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/domain/vendor")
 * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_DOMAIN_ADMIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VendorController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET", "POST"}, name="domain_vendor")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function index(Request $request ,TranslatorInterface $translator , SettingRepository $settingRepository): Response
    {
        $entity = new Vendor();
        $terminal = $this->getUser()->getTerminal()->getId();
        $businessType = $settingRepository->getChildRecords($terminal,"business-type");
        $vendorType = $settingRepository->getChildRecords($terminal,"vendor-type");

        $form = $this->createForm(VendorFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $formValidation = new FormValidationManager();
        $errors = $formValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setTerminal($terminal);
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('domain_vendor');
        }
        return $this->render('domain/vendor/index.html.twig', [
            'businessType' => $businessType,
            'vendorType' => $vendorType,
            'form' => $form->createView(),
        ]);

    }



    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="domain_vendor_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {

        $entity = $this->getDoctrine()->getRepository(Vendor::class)->find($id);
        $form = $this->createForm(VendorFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('post.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('domain_vendor');
        }
        return $this->render('domain/vendor/index.html.twig', [
            'post' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Check if mobile available for registering
     * @Route("/{mode}/available", methods={"GET"}, name="domain_vendor_process_available")
     * @param   string
     * @return  bool
     */
    function isAvailable(Request $request , $mode) : Response
    {
        $data = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $post = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data['vendor_form']);
        return new Response($post);

    }


    /**
     * Check if mobile available for registering
     * @Route("/create-ajax", methods={"POST"}, name="domain_vendor_create_ajax")
     * @param   string
     * @return  bool
     */
    function createAjax(Request $request) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Vendor();
            $post->setTerminal($terminal);
            $vendorType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['vendorType']));
            if ($vendorType) {
                $post->setVendorType($vendorType);
            }
            $post->setName($data['name']);
            $post->setMobile($data['mobile']);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['vatRegistrationNo']){
                $post->setVatRegistrationNo($data['vatRegistrationNo']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="domain_vendor_show")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Vendor::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="domain_vendor_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Vendor::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="doamin_vendor_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */
    public function status($id, SettingRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="domain_vendor_data_table",options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN_ADMIN')")
     */

    public function dataTable(Request $request, VendorRepository $vendorRepository)
    {

        $query = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $iTotalRecords = $this->getDoctrine()->getRepository(Vendor::class)->count(array('terminal'=> $terminal));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $vendorRepository->findBySearchQuery($terminal,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Customer */

        foreach ($result as $post):

            $active = empty($post['status']) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('doamin_vendor_status',array('id'=>$post['id']))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";

            $records["data"][] = array(
                $id                 = $i,
                $businessType       = $post['businessType'],
                $vendorType         = $post['vendorType'],
                $company            = $post['companyName'],
                $name               = $post['name'],
                $mobile             = $post['mobile'],
                $binNo              = $post['location'],
                $vatRegistrationNo  = $post['vatRegistrationNo'],
                $status             = $status,
                $action             ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('domain_vendor_edit',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn btn-mini blue-bg white-font entity-show' data-action='{$this->generateUrl('domain_vendor_show',array('id'=>$post['id']))}' href='javascript:' data-title='{$post['name']}' id='postView-{$post['id']}' ><i class='feather icon-eye'></i> Show</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:' data-action='{$this->generateUrl('domain_vendor_delete',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }
}
