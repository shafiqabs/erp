<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Domain;


use App\Entity\Domain\Domain;
use App\Entity\Domain\EmailTemplate;
use App\Form\Domain\DomainFormType;
use App\Form\Domain\EmailTemplateFormType;
use App\Repository\Domain\EmailTemplateRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/domain/domain-setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DomainController extends AbstractController
{


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/", methods={"GET", "POST"}, name="domain_setting")
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request,TranslatorInterface $translator): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $entity = $this->getDoctrine()->getRepository(Domain::class)->findOneBy(array('terminal'=>$terminal));
        if(empty($entity)){
            $entity =  $this->getDoctrine()->getRepository(Domain::class)->insertDomain($terminal);
        }
        $form = $this->createForm(DomainFormType::class, $entity, array('terminal' => $terminal))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $attachFile = $form->get('file')->getData();
            $entity->upload();
            $entity->printHeaderUpload();
            $entity->printFooterUpload();
            $entity->backgroundImageUpload();
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message); $this->addFlash('success', 'post.updated_successfully');
            return $this->redirectToRoute('domain_setting');
        }
        return $this->render('domain/domain-setting/index.html.twig', [
            'entity' => $entity,
            'terminal' => $terminal->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     * @Route("/database-dump", methods={"GET", "POST"}, name="domain_database_dump")
     * @Security("is_granted('ROLE_DOMAIN')")
     */

    public function databaseDownload()
    {
        $DBUSER="root";
        $DBPASSWD="dhaka123";
        $DATABASE="unifill81";
        $filename = "unifill-" . date("d-m-Y") . ".sql.gz";
        $mime = "application/x-gzip";
        header( "Content-Type: " . $mime );
        header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
        $cmd = "mysqldump -u $DBUSER --password=$DBPASSWD $DATABASE | gzip --best";
        passthru( $cmd );
        exit(0);

    }

}
