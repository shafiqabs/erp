<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;


use App\Entity\Admin\Location;
use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Profile;
use App\Entity\Core\UserReporting;
use App\Entity\User;
use App\Form\Core\EditProfileFormType;
use App\Form\Core\ProfileFormType;
use App\Form\Core\RegistrationFormType;
use App\Form\Core\UpdateProfileFormType;
use App\Form\Type\ChangePasswordType;
use App\Repository\Core\UserReportingRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/profile/user")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProfileController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_USER')")
     * @Route("/update", methods={"GET", "POST"}, name="profile_update")
     */
    public function update(Request $request, TranslatorInterface $translator): Response
    {
        $post = $this->getUser();
        $profile = $this->getUser()->getProfile();
        $terminal = $post->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(UpdateProfileFormType::class, $post->getProfile(), array('terminal' => $terminal,'userRepo' => $userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $fileUpload = $form->get('file')->getData();
            if($fileUpload){
                $profile->removeUpload();
            }
            $profile->upload();
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('profile_update');
        }
        return $this->render('core/profile/updateProfile.html.twig', [
            'entity' => $post,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_USER')")
     * @Route("/change-password", methods={"GET", "POST"}, name="change_password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class) ->add('ResetPassword', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('security_logout');
        }
        return $this->render('core/profile/changePassword.html.twig', [
            'entity' => $user,
            'form' => $form->createView(),
        ]);
    }


}
