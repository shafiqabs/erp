<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApplicationController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/admin", methods={"GET"}, name="app_admin")
     */
    public function index(): Response
    {

        return $this->render('dashboard/dashboard.html.twig');
    }


     /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
     * @Route("/core", methods={"GET"}, name="app_core")
     */
    public function indexCore(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }


    /**
     * Lists all Post entities.
     * @Route("/accounting", methods={"GET"}, name="app_accounting")

     */
    public function indexAccounting(): Response
    {
        return $this->render('@TerminalbdAccounting/default/menu.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/inventory-menu", methods={"GET"}, name="app_header_menu")
     */
    public function inventoryMenu()
    {
        return $this->render('@TerminalbdInventory/default/menu.html.twig');
    }

     /**
     * Lists all Post entities.
     * @Route("/garments-menu", methods={"GET"}, name="app_header_menu")
     */
    public function garmentsMenu()
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        return $this->render('@TerminalbdGarments/default/menu.html.twig',['terminal'=>$terminal]);
    }

     /**
     * Lists all Post entities. m
     * @Route("/bank-menu", methods={"GET"}, name="app_header_menu")
     */
    public function bankMenu()
    {
        return $this->render('@TerminalbdBank/default/menu.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/budget-menu", methods={"GET"}, name="app_header_menu")
     */
    public function budgetMenu()
    {
        return $this->render('@TerminalbdBudget/default/menu.html.twig');
    }


     /**
     * Lists all Post entities.
     * @Route("/procurement-menu", methods={"GET"}, name="app_header_menu")
     */
    public function procurementMenu()
    {
        return $this->render('@TerminalbdProcurement/default/menu.html.twig');
    }

     /**
     * Lists all Post entities.
     * @Route("/procurement-report-menu", methods={"GET"}, name="app_header_menu")
     */
    public function procurementReportMenu()
    {
        return $this->render('@TerminalbdProcurement/default/report.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/security-billing-menu", methods={"GET"}, name="app_header_menu")
     */
    public function paymentMenu()
    {
        return $this->render('@TerminalbdAccounting/default/menu.html.twig');
    }



}
