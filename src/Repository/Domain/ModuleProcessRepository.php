<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ModuleProcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModuleProcess::class);
    }

    public function insertModule(Terminal $terminal)
    {
        $em = $this->_em;
        $entities = $em->getRepository(AppModule::class)->getSpecificModule($terminal);
        foreach ($entities as $entity){
            $exist = $this->findOneBy(array('terminal' => $terminal,'module' => $entity));
            if(empty($exist)){
               $module = new ModuleProcess();
                $module->setTerminal($terminal->getId());
                $module->setModule($entity);
                $module->setStatus(1);
                $em->persist($module);
                $em->flush();
            }
        }

    }

    public function existModuleProcess($terminal,$module)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.module','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;

    }



}
