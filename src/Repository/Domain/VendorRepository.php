<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class VendorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vendor::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $Vendor = $qb->delete(Vendor::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($Vendor){
            $Vendor->execute();
        }
    }


    public function checkAvailable($terminal , $mode,$data)
    {
        $process = "true";
        $mobile = isset($data['mobile']) ? $data['mobile'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->where('e.terminal =:terminal')->setParameter("terminal",$terminal);
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$data)
    {

        $name = isset($data['name'])? $data['name'] :'';
        $username = isset($data['username'])? $data['username'] :'';
        $email = isset($data['email'])? $data['email'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';

        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($username)){
            $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
        }
        if(!empty($email)){
            $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }

    }

    public function systemuserCreate( Terminal $terminal, UserPasswordEncoderInterface $encoder ){

        $em = $this->_em;
        $user = new User();
        $user->setTerminal($terminal);
        $user->setName($terminal->getOrganizationName());
        $user->setUsername($terminal->getMobile());
        $user->setMobile($terminal->getMobile());
        if($terminal->getEmail()){
            $user->setEmail($terminal->getEmail());
        }else{
            $email = $terminal->getMobile()."@gmail.com";
            $user->setEmail($email);
        }
        $user->setPassword($encoder->encodePassword($user,"*148148#"));
        $user->setRoles(array('ROLE_DOMAIN'));
        $em->persist($user);
        $em->flush();
    }


    /**
     * @return Vendor[]
     */
    public function findBySearchQuery($data )
    {

        $qb = $this->createQueryBuilder('e');
        $qb->where('e.terminal IS NOT NULL');
        $keyword = isset($data['requisition_filter_form']['keyword'])? $data['requisition_filter_form']['keyword'] :'';
        if(!empty($keyword)){
            $qb->andWhere($qb->expr()->like("e.companyName", "'%$keyword%'"));
        }
        $qb->orderBy('e.companyName', 'ASC');
        $result = $qb->getQuery();
        return $result;
    }

}
