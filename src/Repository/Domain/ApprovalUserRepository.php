<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;

use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetGlAdditional;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlip;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ApprovalUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApprovalUser::class);
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $ApprovalUser = $qb->delete(ApprovalUser::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($ApprovalUser){
            $ApprovalUser->execute();
        }
    }

    public function getDepartmentApprovalAssignUser($terminal ,$requisition)
    {

        if(!empty($requisition->getModule()) and !empty($requisition->getDepartment())){
            $module = $requisition->getModule();
            $department = $requisition->getDepartment()->getId();
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->leftJoin('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if($requisition->getModule()){
                $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
            }
            if($requisition->getDepartment()){
                $qb->andWhere('d.id = :id')->setParameter('id',$department);
            }
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getModuleApprovalBudgetUser($terminal ,$requisition)
    {

        if(!empty($requisition->getModule())){
            $module = $requisition->getModule();
            $department = $requisition->getDepartment()->getId();
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if($requisition->getModule()){
                $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
            }
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignUserRequisitionIssue($terminal ,$requisition)
    {
        /** @var $module ModuleProcess */
        $module = $requisition->getModuleProcess();
        if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->leftJoin('e.branches','br');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
                $qb->andWhere('b.id = :mid')->setParameter('mid',$module->getModule()->getId());
            }
            if($requisition->getProcessDepartment()){
                $department = $requisition->getProcessDepartment()->getId();
                $qb->andWhere('d.id = :did')->setParameter('did',$department);
            }
            if($requisition->getBranch()){
                $branch = $requisition->getBranch()->getId();
                $qb->andWhere('br.id = :bid')->setParameter('bid',$branch);
            }
            $qb->orderBy('e.ordering','ASC');
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignUser($terminal ,$requisition)
    {
        /** @var $module ModuleProcess */
        $module = $requisition->getModuleProcess();
        if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->leftJoin('e.branches','br');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
                $qb->andWhere('b.id = :mid')->setParameter('mid',$module->getModule()->getId());
            }
            if(!empty($module->getOperationGroup()) and $module->getOperationGroup() == 'department' and !empty($requisition->getProcessDepartment())){
                $department = $requisition->getProcessDepartment()->getId();
                $qb->andWhere('d.id = :did')->setParameter('did',$department);
            }elseif(!empty($module->getOperationGroup()) and $module->getOperationGroup() == 'branch' and !empty($requisition->getBranch())){
                $branch = $requisition->getBranch()->getId();
                $qb->andWhere('br.id = :bid')->setParameter('bid',$branch);
            }
            $qb->orderBy('e.ordering','ASC');
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignPaymentUser($terminal ,$requisition)
    {
        /** @var $module ModuleProcess */
        $module = $requisition->getModuleProcess();
        if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->leftJoin('e.branches','br');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
                $qb->andWhere('b.id = :mid')->setParameter('mid',$module->getModule()->getId());
            }
            if($requisition->getCompany()){
                $branch = $requisition->getCompany()->getId();
                $qb->andWhere('br.id = :bid')->setParameter('bid',$branch);
            }
            $qb->orderBy('e.ordering','ASC');
            $result = $qb->getQuery()->getResult();
            return $result;

        }
        return false;
    }

    public function getApprovalAssignUserRequisitionSlip($terminal , RequisitionSlip $requisition)
    {
        /** @var $module ModuleProcess */
        $module = $requisition->getModuleProcess();
        if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.user','u');
            $qb->join('e.appModules','b');
            $qb->leftJoin('e.departments','d');
            $qb->leftJoin('e.branches','br');
            $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
            if(!empty($module->getApproveType()) and $module->getApproveType() == 'user'){
                $qb->andWhere('b.id = :mid')->setParameter('mid',$module->getModule()->getId());
            }
            if($requisition->getWaitingProcess()){
                $department = $requisition->getProcessDepartment()->getId();
                $qb->andWhere('d.id = :did')->setParameter('did',$department);
            }
            if($requisition->getCompany()){
                $branch = $requisition->getCompany()->getId();
                $qb->andWhere('br.id = :bid')->setParameter('bid',$branch);
            }
            $qb->orderBy('e.ordering','ASC');
            $result = $qb->getQuery()->getResult();
            return $result;


        }
        return false;
    }

    public function getApprovalAssignMatrixUser($terminal , Requisition $requisition)
    {
        /** @var $module ModuleProcess */
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user', 'u');
        // $qb->leftJoin('e.appModules', 'b');
        $qb->leftJoin('e.departments', 'd');
        $qb->leftJoin('e.branches', 'br');
        $qb->select('fm.amount');
        $qb->addSelect('u.id as uid');
        $qb->addSelect('e.process as process','e.ordering as ordering');
        $qb->where('e.terminal = :terminal')->setParameter('terminal', $terminal);
        $department = $requisition->getProcessDepartment()->getId();
        $qb->andWhere('d.id = :did')->setParameter('did', $department);
        $branch = $requisition->getCompanyUnit()->getParent()->getId();
        $qb->andWhere('br.id = :bid')->setParameter('bid', $branch);
        if ($requisition->getRequisitionMode() == "CAPEX"){
            $qb->join('e.financialMatrixCapex', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "OPEX"){
            $qb->join('e.financialMatrixOpex', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "Expense" and $requisition->getExpenseBillType()->getSlug() =="expense"){
            $qb->join('e.financialMatrixExpense', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "Expense" and $requisition->getExpenseBillType()->getSlug() =="aap"){
            $qb->join('e.financialMatrixAap', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "Expense" and $requisition->getExpenseBillType()->getSlug() =="oar"){
            $qb->join('e.financialMatrixOar', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "Confidential"){
            $qb->join('e.financialMatrixConfidential', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }elseif ($requisition->getRequisitionMode() == "INV"){
            $qb->join('e.financialMatrixInv', 'fm');
            $qb->andWhere("fm.amount <= {$requisition->getTotal()}");
        }
        $qb->orderBy('e.ordering','ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getApprovalAssignMatrixForBudgetUser($terminal , BudgetGlAdditional $requisition)
    {
        /** @var $module ModuleProcess */
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user', 'u');
        $qb->leftJoin('e.departments', 'd');
        $qb->leftJoin('e.branches', 'br');
        $qb->select('fm.amount');
        $qb->addSelect('u.id as uid');
        $qb->addSelect('e.process as process','e.ordering as ordering');
        $qb->where('e.terminal = :terminal')->setParameter('terminal', $terminal);
        $branch = $requisition->getCompany()->getId();
        $qb->andWhere('br.id = :bid')->setParameter('bid', $branch);
        $qb->join('e.financialMatrixAdditionalBudget', 'fm');
        $qb->andWhere("fm.amount <= {$requisition->getAmount()}");
        $qb->orderBy('e.ordering','ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function getCheckModule($terminal , $module)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($module){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
        }
        $result = $qb->getQuery()->getResult();
        return $result;

    }

     public function getModuleApprovalAssignUser($terminal , $module = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($module){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$module);
        }
        $result = $qb->getQuery()->getResult();
        return $result;

    }

    public function getModuleApprovalUser($terminal , $bundle = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.appModules','b');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($bundle){
            $qb->andWhere('b.slug = :slug')->setParameter('slug',$bundle);
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }
}
