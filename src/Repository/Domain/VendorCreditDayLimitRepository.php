<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Domain;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\Domain\VendorCreditDayLimit;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class VendorCreditDayLimitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VendorCreditDayLimit::class);
    }

    public function companyCreditLimit(Vendor $vendor){

        $em = $this->_em;
        $companies = $em->getRepository(Branch::class)->getCompanyUnit('branch');
        foreach ($companies as $company):
            $exist = $this->findOneBy(array('company'=>$company['id'],'vendor' => $vendor));
            if(empty($exist)){
                $entity = new VendorCreditDayLimit();
                $entity->setCompany($em->getRepository(Branch::class)->find($company['id']));
                $entity->setVendor($vendor);
                $entity->setLimitDay(20);
                $em->persist($entity);
            }
        endforeach;
        $em->flush();
    }

    public function limitUpdate($data){

        $em = $this->_em;
        $limits = ($data['limitDay']);
        foreach ($limits as $key => $limit):
            $entity = $this->find($key);
            if($entity){
                $entity->setLimitDay($limit);
                $em->persist($entity);
            }
        endforeach;
        $em->flush();
    }

    public function systemDelete($terminal)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $Vendor = $qb->delete(Vendor::class, 'e')->where('e.terminal = ?1')->setParameter(1, $terminal)->getQuery();
        if($Vendor){
            $Vendor->execute();
        }
    }




    public function checkAvailable($terminal , $mode,$data)
    {
        $process = "true";
        $mobile = isset($data['mobile']) ? $data['mobile'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->where('e.terminal =:terminal')->setParameter("terminal",$terminal);
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$data)
    {

        $name = isset($data['name'])? $data['name'] :'';
        $username = isset($data['username'])? $data['username'] :'';
        $email = isset($data['email'])? $data['email'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';

        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($username)){
            $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
        }
        if(!empty($email)){
            $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }

    }

    /**
     * @return Vendor[]
     */
    public function findBySearchQuery( $terminal, $parameter , $data ): array
    {


        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.businessType','bt');
        $qb->leftJoin('e.vendorType','vt');
        $qb->leftJoin('e.location','l');
        $qb->select('e.id as id','e.name as name','e.mobile as mobile','e.email as email','e.companyName as companyName','e.vatRegistrationNo as vatRegistrationNo','e.binNo as binNo','e.registrationNo as registrationNo');
        $qb->addSelect('bt.name as businessType');
        $qb->addSelect('vt.name as vendorType');
        $qb->addSelect('l.name as location');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

}
