<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;

use App\Entity\Core\Setting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class SettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Setting::class);
    }

    public function getChildRecords($terminal , $parent)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.settingType','p');
        $qb->leftJoin('e.location','l');
        $qb->select('e.id as id','e.name as name','e.code as code','e.status as status','e.contactPerson as contactMobile','e.contactPerson as contactPerson','e.address as address');
        $qb->addSelect('l.name as location');
        $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('p.slug = :slug')->setParameter('slug',$parent);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function departmentWiseGL($terminal)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join("e.settingType","st");
        $qb->where("st.slug ='department'");
        $qb->andWhere("e.terminal ='{$terminal}'");
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getResult();
        return $result;
    }


}
