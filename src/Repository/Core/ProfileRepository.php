<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;

use App\Entity\Core\Profile;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    public function insertDomainUser(User $user)
    {
        $em = $this->_em;
        $profile = new Profile();
        $profile->setName($user->getName());
        $profile->setUser($user);
        $profile->setMobile($user->getTerminal()->getMobile());
        $profile->setEmail($user->getTerminal()->getEmail());
        $em->persist($profile);
        $em->flush();
    }

    public function getDepartmentUsers($user,$department){

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->select('u.id','u.name as name');
        $qb->addSelect('e.employeeId as employeeId');
        $qb->where('e.department =:d')->setParameter('d',$department);
        $qb->andWhere('u.id !=:uid')->setParameter('uid',$user);
        $qb->andWhere('u.enabled =1');
        $qb->orderBy('u.username','ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getDepartmentProfiles($terminal,$department){

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.branch','c');
        $qb->join('e.designation','d');
        $qb->select('u.id','u.name as name');
        $qb->addSelect('e.employeeId as employeeId','e.mobile as mobile');
        $qb->addSelect('c.name as companyName','c.code as code');
        $qb->addSelect('d.name as designation');
        $qb->where('e.department =:d')->setParameter('d',$department);
        $qb->andWhere('u.terminal =:terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('u.enabled =1');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

}
