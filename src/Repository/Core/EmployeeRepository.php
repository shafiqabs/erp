<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;

use App\Entity\Core\Employee;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class EmployeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    protected function handleSearchBetween($qb,$form)
    {

        if (isset($form['filter_form'])) {
            $data = $form['filter_form'];
            $employeeId = trim(isset($data['employeeId']) ? $data['employeeId'] : '');
            $name = trim(isset($data['name']) ? $data['name'] : '');
            $username = trim(isset($data['username']) ? $data['username'] : '');
            $email = trim(isset($data['email']) ? $data['email'] : '');
            $mobile = trim(isset($data['mobile']) ? $data['mobile'] : '');
            $department = trim(isset($data['department']) ? $data['department'] : '');
            $designation = isset($data['designation']) ? $data['designation'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $reportTo = isset($data['reportTo']) ? $data['reportTo'] : '';
            $relieverTo = isset($data['relieverTo']) ? $data['relieverTo'] : '';
            if (!empty($employeeId)) {
                $qb->andWhere($qb->expr()->like("e.employeeId", "'%$employeeId%'"));
            }
            if (!empty($branch)) {
                $qb->andWhere('e.company =:branch')->setParameter('branch', $branch);
            }
            if (!empty($department)) {
                $qb->andWhere('e.department =:department')->setParameter('department', $department);
            }
            if (!empty($designation)) {
                $qb->andWhere('e.designation =:designation')->setParameter('designation', $designation);
            }
            if (!empty($name)) {
                $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
            }
            if (!empty($email)) {
                $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
            }
            if (!empty($mobile)) {
                $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
            }
        }

    }


    public function findWithSearchQuery($data )
    {
        $sort = isset($data['sort']) ? $data['sort'] : 'e.employeeId';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.designation','dg');
        $qb->leftJoin('e.department','dp');
        $qb->leftJoin('e.section','s');
        $qb->leftJoin('e.category','c');
        $qb->leftJoin('e.company','b');
        $qb->select('e.id as id','e.name as name','e.email as email','e.bankAccountNo as bankAccountNo','e.bankName as bankName','e.status as enabled','e.ebl as ebl');
        $qb->addSelect('e.mobile as mobile','e.employeeId as employeeId');
        $qb->addSelect('b.name as branch','b.id as branchId');
        $qb->addSelect('dg.id as designationId','dg.name as designation');
        $qb->addSelect('dp.id as departmentId','dp.name as department');
        $qb->addSelect('s.id as sectionId','s.name as section');
        $qb->addSelect('c.id as categoryId','c.name as category');
        $qb->where('e.employeeId IS NOT NULL');
        $qb->andWhere('e.isDelete =0');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}", "$direction");
        $result = $qb->getQuery();
        return $result;
    }

    public function getDepartmentUsers($user,$department){

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->select('u.id','u.name as name');
        $qb->addSelect('e.employeeId as employeeId');
        $qb->where('e.department =:d')->setParameter('d',$department);
        $qb->andWhere('u.id !=:uid')->setParameter('uid',$user);
        $qb->andWhere('u.enabled =1');
        $qb->orderBy('u.username','ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getDepartmentProfiles($terminal,$department){

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.user','u');
        $qb->join('e.branch','c');
        $qb->join('e.designation','d');
        $qb->select('u.id','u.name as name');
        $qb->addSelect('e.employeeId as employeeId','e.mobile as mobile');
        $qb->addSelect('c.name as companyName','c.code as code');
        $qb->addSelect('d.name as designation');
        $qb->where('e.department =:d')->setParameter('d',$department);
        $qb->andWhere('u.terminal =:terminal')->setParameter('terminal',$terminal);
        $qb->andWhere('u.enabled =1');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function employeeSearchable($data)
    {
        if(isset($data['term']) and !empty($data['term'])){
            $q = $data['term'];
            $qb = $this->createQueryBuilder('e');
            $qb->select('e.id as id');
            $qb->addSelect("CONCAT(e.employeeId,' - ',e.name)  as text");
            $qb->where('e.status = 1');
            $qb->andWhere('e.name LIKE :searchTerm OR e.employeeId LIKE :searchTerm');
            $qb->setParameter('searchTerm', '%'.trim($q).'%');
            $qb->setMaxResults(100);
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return false;

    }

}
