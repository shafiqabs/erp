<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Application\Budget;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetAmendment;
use Terminalbd\BudgetBundle\Entity\BudgetGlAdditional;
use Terminalbd\BudgetBundle\Entity\BudgetProcess;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisitionAmendment;
use Terminalbd\BudgetBundle\Entity\BudgetYear;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BudgetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Budget::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function reset($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $BudgetProcess = $qb->delete(BudgetProcess::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetProcess){
                $BudgetProcess->execute();
            }

            $BudgetRequisitionAmendment = $qb->delete(BudgetRequisitionAmendment::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetRequisitionAmendment){
                $BudgetRequisitionAmendment->execute();
            }

            $BudgetAmendment = $qb->delete(BudgetAmendment::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetAmendment){
                $BudgetAmendment->execute();
            }

            $BudgetRequisition = $qb->delete(BudgetRequisition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetRequisition){
                $BudgetRequisition->execute();
            }

            $BudgetAdditional = $qb->delete(BudgetGlAdditional::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetAdditional){
                $BudgetAdditional->execute();
            }

            $BudgetAmendment = $qb->delete(BudgetYear::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetAmendment){
                $BudgetAmendment->execute();
            }
        }

    }

    public function remove($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $BudgetProcess = $qb->delete(BudgetProcess::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetProcess){
                $BudgetProcess->execute();
            }

            $BudgetRequisitionAmendment = $qb->delete(BudgetRequisitionAmendment::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetRequisitionAmendment){
                $BudgetRequisitionAmendment->execute();
            }
            $BudgetAmendment = $qb->delete(BudgetAmendment::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetAmendment){
                $BudgetAmendment->execute();
            }

            $BudgetRequisition = $qb->delete(BudgetRequisition::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetRequisition){
                $BudgetRequisition->execute();
            }

            $BudgetYear = $qb->delete(BudgetYear::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($BudgetYear){
                $BudgetYear->execute();
            }

            $Head = $qb->delete(Head::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Head){
                $Head->execute();
            }

        }
        if($con and $process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }



}
