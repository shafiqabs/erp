<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Application;

use App\Entity\Application\Accounting;
use App\Entity\Application\Billing;
use App\Entity\Application\Procurement;
use App\Entity\Application\SecurityBilling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Terminalbd\SecurityBillingBundle\Entity\DeploymentPost;
use Terminalbd\SecurityBillingBundle\Entity\Invoice;
use Terminalbd\SecurityBillingBundle\Entity\InvoiceBatch;
use Terminalbd\SecurityBillingBundle\Entity\Particular;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SecurityBillingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SecurityBilling::class);
    }

    public function config($terminal)
    {
        $config = $this->findOneBy(array('terminal' => $terminal));
        return $config;
    }

    public function reset($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $Invoice = $qb->delete(Invoice::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Invoice){ $Invoice->execute();}

            $InvoiceBatch = $qb->delete(InvoiceBatch::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($InvoiceBatch){ $InvoiceBatch->execute();}

            $DeploymentPost = $qb->delete(DeploymentPost::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($DeploymentPost){ $DeploymentPost->execute();}

            $Particular = $qb->delete(Particular::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Particular){ $Particular->execute();}
        }

    }

    public function remove($terminal, $process = ""){

        $em = $this->_em;
        $con = $this->findOneBy(array('terminal' => $terminal));
        if($con) {

            $qb = $em->createQueryBuilder();
            $config = $con->getId();

            $Invoice = $qb->delete(Invoice::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Invoice){ $Invoice->execute();}

            $InvoiceBatch = $qb->delete(InvoiceBatch::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($InvoiceBatch){ $InvoiceBatch->execute();}

            $DeploymentPost = $qb->delete(DeploymentPost::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($DeploymentPost){ $DeploymentPost->execute();}

            $Particular = $qb->delete(Particular::class, 'e')->where('e.config = ?1')->setParameter(1, $config)->getQuery();
            if($Particular){ $Particular->execute();}
        }
        if($con and $process == "remove"){
            $em->remove($con);
            $em->flush();
        }

    }

}
