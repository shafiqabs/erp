<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Admin;

use App\Entity\Admin\AppBundle;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Setting;
use App\Entity\Admin\Terminal;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class TerminalType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true , 'class' => ''],
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Enter the organization/company name',
                    ]),
                ],
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true , 'class' => ''],
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Enter the organization/company mobile no',
                    ]),
                ],
            ])
            ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
            ])

            ->add('displayMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'mobileLocal'],
                'required' => false,
            ])

            ->add('address', TextareaType::class, [
                'attr' => ['autofocus' => true,'class' => 'address'],
                'required' => false,
            ])
              ->add('domain', TextType::class, [
                   'attr' => ['autofocus' => true],
                   'required' => false,
               ])
                ->add('binNo', TextType::class, [
                    'attr' => ['autofocus' => true],
                    'required' => false,
                ])
                ->add('tinNo', TextType::class, [
                    'attr' => ['autofocus' => true],
                    'required' => false,
                ])
               ->add('mainApp', EntityType::class, [
                   'class' => AppBundle::class,
                   'required' => true,
                   'query_builder' => function (EntityRepository $er) {
                       return $er->createQueryBuilder('e')
                           ->orderBy('e.name', 'ASC');
                   },
                   'constraints' => [
                       new NotBlank([
                           'message' => 'Please choose a main app',
                       ]),
                   ],
                   'attr'=>['class'=>'select2'],
                   'choice_label' => 'name',
                   'placeholder' => 'Choose a main app',
               ])
               ->add('businessGroup', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","t")
                        ->where("e.status = 1")
                        ->andWhere("t.slug = 'business-group'")
                        ->orderBy('e.name', 'ASC');
                },
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please choose a business group',
                    ]),
                ],
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a business group',
            ])
            ->add('approveProcess',CheckboxType::class,[
                'required' => false,
                'label' => 'Approve Process',
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
            ->add('appBundles', EntityType::class, [
                   'class' => AppBundle::class,
                   'required'      => true,
                   'expanded'      => true,
                   'multiple'      => true,
                   'constraints' => [
                       new NotBlank([
                           'message' => 'Enter application module',
                       ]),
                   ],
                   'query_builder' => function (EntityRepository $er) {
                       return $er->createQueryBuilder('e')
                           ->where("e.status = 1")
                           ->orderBy('e.name', 'ASC');
                   },
                   'attr'=>['class'=>'checkbox'],
                   'choice_label' => 'name',
                   'label_attr' => ['class' => 'checkbox']
               ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Terminal::class,

        ]);
    }


}