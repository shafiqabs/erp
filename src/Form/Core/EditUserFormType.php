<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Core;


use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Setting;
use App\Entity\Domain\BundleRoleGroup;
use App\Entity\User;
use App\Repository\Admin\LocationRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Types\ArrayType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class EditUserFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $terminal =  $options['terminal']->getId();
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'autofocus' => true,'class'=>'inputs'],
                'required' => true,
            ])

            ->add('email', EmailType::class, [
                'attr' => ['autofocus' => true,'class'=>'inputs'],
                'required' => true,
            ])
            ->add('userGroup', EntityType::class, array(
                'required'    => true,
                'class' => Setting::class,
                'placeholder' => 'Choose an  user group',
                'choice_label' => 'name',
                'attr'=>array('class'=>'inputs'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='user-group'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('userGroupRole', EntityType::class, [
                'class' => BundleRoleGroup::class,
                'multiple' => false,
                'group_by'  => 'appBundle.name',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a bundle role',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where('e.terminal = :terminal')->setParameter('terminal', $terminal)
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('enabled',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
            ->add('roles', ArrayType::class, [
                'multiple' => true,
                'choices'   => $options['userRepo']->getAccessRoleGroup($options['terminal'])
            ]);

     }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'terminal' => Terminal::class,
            'userRepo' => UserRepository::class,
        ]);
    }


}