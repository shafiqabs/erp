<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Domain;


use App\Entity\Admin\AppModule;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Entity\Domain\ModuleProcess;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Terminal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ModuleProcessFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // $bundle =  $options['terminal']->getBundleIds();
        $bundle =  $options['terminal']->getAppBundles();
        $builder
            ->add('module', EntityType::class, [
                'class' => AppModule::class,
                'multiple' => false,
                'group_by'  => 'appBundle.name',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a app module',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($bundle){
                    return $er->createQueryBuilder('e')
                        ->join('e.appBundle','b')
                        ->where('b.id IN (:bundle)')->setParameter('bundle', $bundle)
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('endModule', EntityType::class, [
                'class' => AppModule::class,
                'multiple' => false,
                'required' => false,
                'group_by'  => 'appBundle.name',
                'choice_label'  => 'name',
                'attr'=>['class'=>'span12'],
                'placeholder' => 'Choose a app module',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($bundle){
                    return $er->createQueryBuilder('e')
                        ->join('e.appBundle','b')
                        ->where('b.id IN (:bundle)')->setParameter('bundle', $bundle)
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('endStatus', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                'required' => false,
            ])

            ->add('approveType', ChoiceType::class, [
                'multiple' => false,
                'required' => false,
                'expanded' => false,
                'placeholder' => 'Select approve type',
                'choices' => [
                    'User Base' => 'user',
                    'Role Base' => 'role'
                ],
                'attr' => [
                    'class' => 'form-check-inline'
                ]
            ])

            ->add('operationGroup', ChoiceType::class, [
                'multiple' => false,
                'required' => false,
                'expanded' => false,
                'placeholder' => 'Select operation group',
                'choices' => [
                    'Company' => 'branch',
                    'Department' => 'department',
                ],
                'attr' => [
                    'class' => 'form-check-inline'
                ]
            ])

            ->add('attachment',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

            ->add('approverForm',CheckboxType::class,[
                'required' => false,
                'label' => 'Approver Form',
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

            ->add('approverReject',CheckboxType::class,[
                'required' => false,
                'label' => 'Approver Reject',
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModuleProcess::class,
            'terminal' => Terminal::class,
        ]);
    }
}
