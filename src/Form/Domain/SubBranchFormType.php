<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Domain;


use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SubBranchFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
       $terminal =  $options['terminal']->getId();

        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
            ])
            ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
            ])
            ->add('contactPerson', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
            ])

            ->add('contactPhone', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
            ])

            ->add('parent', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'group_by'  => 'customer.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.branchType ='branch'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere("e.terminal ={$terminal}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a branch name',
            ])


            ->add('location', EntityType::class, [
                'class' => Location::class,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.level =3')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a branch location',
            ])

            ->add('code', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                 'required' => false,

            ])

            ->add('address', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea'],
                 'required' => false,

            ])
            ->add('billingAddress', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea'],
                 'required' => false,

            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Branch::class,
            'terminal'  => Terminal::class,
        ]);

    }
}
