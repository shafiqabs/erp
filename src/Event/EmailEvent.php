<?php
/**
 * Created by PhpStorm.
 * User: dhaka
 * Date: 8/19/14
 * Time: 5:24 PM
 */

namespace App\Event;

use App\Entity\Domain\EmailTemplate;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;


class EmailEvent extends Event
{

    /** @var EmailTemplate */
    protected $emailTemplate;

    /** @var array */
    protected $emailData;

    public function __construct(EmailTemplate $emailTemplate,$emailData)
    {
        $this->emailTemplate = $emailTemplate;
        $this->emailData = $emailData;
    }


    /**
     * @return EmailTemplate
     */
    public function getEmailTemplate()
    {
        return $this->emailTemplate;
    }

    /**
     * @return array
     */
    public function getEmailData()
    {
        return $this->emailData;
    }


}